// create a library of images to draw from
var randomImageLibrary = [
    {
        imageUrl: 'images/hero-image-2.webp'
    }
];

// pick random image from library to use on page
var randomImageIndex = Math.floor(Math.random()* randomImageLibrary.length );

// update hero element with new image and to be credit
var currentStyles = document.getElementById("slim-hero").getAttribute("style");
document.getElementById("slim-hero")
.setAttribute("style", `background: url(${ randomImageLibrary[randomImageIndex].imageUrl  });` +
'background-size: cover;  background-position: center top;'
);
