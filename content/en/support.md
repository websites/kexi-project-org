---
title: "Support"
layout: simple
date: '2024-12-12'
menu:
  main:
    weight: 3
scssFiles:
- scss/support.scss
---

<style>
    em  {
      opacity: 0.7;
    }
</style>

The KEXI project provides a variety of resources to assist users in effectively using and contributing to the application. Documentation, including the KEXI Handbook, FAQ, and tutorials, offers detailed guidance on functionality, workflows, and best practices. Additionally, users can report bugs, suggest features, and review privacy policies, ensuring a collaborative and secure experience with KEXI.

<em>
Note: Some materials cover older KEXI versions and will be updated.
</em>

<div class="row col-md-12">
  <div class="col-md-6">
    <h3>{{< i18n "faq-long" >}}</h3>
    <p>The KEXI FAQ section provides essential information about the application's functionality and usage. It covers topics such as general features, common mistakes to avoid, supported file formats, and database migration processes. Additional sections include details on scripting capabilities, development tools, and opportunities for contributions to the KEXI project.
    </p>
    <a href="https://community.kde.org/Kexi/FAQ" target="_blank" class="button ghost">{{< i18n "read-faq" >}}</a>
  </div>
  <div class="col-md-6">
    <h3>{{< i18n "contact" >}}</h3>
    <p>KEXI users and contributors have many ways to connect with the community and share feedback. They can join discussions, report bugs, or share ideas on the KDE Discuss forums, fostering active participation in KEXI’s development. Social media, chat groups, and mailing lists also enable users to stay informed, exchange ideas, and collaborate with others in real time.
    </p>
    <a href="{{< ref "/contact" >}}" class="button ghost">{{< i18n "contact" >}}</a>
  </div>
</div>

<div class="row col-md-12">
  <div class="col-md-6">
    <h3>{{< i18n "documentation" >}}</h3>
    <p>The KEXI Handbook provides comprehensive guidance on using the KEXI application, covering topics such as creating, opening, and managing data, designing tables, forms, queries, and reports, and configuring the application interface. It includes appendices with a glossary, an introduction to databases, references for commands, data types, SQL syntax, and file formats.
    </p>
    <a href="https://userbase.kde.org/Kexi/Handbook" target="_blank" class="button ghost">{{< i18n "handbook" >}}</a>
    <a href="https://docs.kde.org/stable5/en/kexi/kexi/kexi.pdf" target="_blank" class="button ghost">PDF</a>
  </div>
  <div class="col-md-6">
    <h3>{{< i18n "file-a-bug" >}}</h3>
    <p>KEXI users can improve the app significantly by reporting bugs, suggesting features, or providing valuable feedback. The KDE Bug Tracking System (Bugzilla) ensures accurate and helpful reports. Specific instructions are provided for reporting crashes. Users are also encouraged to verify existing bug reports and thoroughly test new features to enhance KEXI's development.
    </p>
    <a href="https://community.kde.org/Kexi/File_a_bug_or_wish" target="_blank" class="button ghost">{{< i18n "more-info" >}}</a>
    <a href="https://bugs.kde.org/enter_bug.cgi?product=Kexi&amp;format=guided" target="_blank" class="button ghost">{{< i18n "open-bug-tracker" >}}</a>
  </div>
</div>

<div class="row col-md-12">
  <div class="col-md-6">
    <h3>{{< i18n "tutorials" >}}</h3>
    <p>Tutorials provide practical guides for mastering key features and workflows in KEXI. These include step-by-step instructions for importing and analyzing data, creating parameter queries, designing reports, and integrating SQLite databases. These resources showcase KEXI's flexibility and capability in managing and presenting data.
    </p>
    <a href="https://userbase.kde.org/Kexi/Tutorials" target="_blank" class="button ghost">{{< i18n "tutorials" >}}</a>
  </div>
  <div class="col-md-6">
    <h3>{{< i18n "privacy" >}}</h3>
    <p>The KEXI Privacy section outlines the application's commitment to maintaining user privacy. It explains that KEXI does not collect or share personal information, and ensures that user data remains private. Additionally, the policy addresses privacy practices on the KEXI website, emphasizing the absence of third-party tracking or advertisements.
    </p>
    <a href="{{< ref "/privacy-statement" >}}" class="button ghost">{{< i18n "privacy-statement" >}}</a>
  </div>
  <div class="col-md-6">
  </div>
</div>
