---
title: "KEXI 2.9 Released"
date: "2015-02-26"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 2.9 is focused on improving the quality of existing features. Thanks to close collaboration with users of KEXI, about 150 changes have been performed.__

## Highlights

KEXI 2.9.0 fixes editor sizes in Table Views, combo box popups, vertical header updates, insertion, and cursor movement. Usability improvements ensure smoother scrolling during edits. Row editing and the scrollbar behavior has been optimized. Column width management has been improved in queries, as well as support for the LIKE operators. Reports now support Longitude, Latitude, and Zoom properties for maps. Import/Export functionality is optimized, including fixes for OpenDocument Spreadsheet files import and export of parameterized queries to CSV.

## Details & Download

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases/Kexi_2.9.

This release offers source code downloads and installers for 64bit MS Windows. For versions for other systems ask your Linux distributor. The source code is known to build and run with Linux and MS Windows.

{{< see-download >}}

{{< see-release-history >}}
