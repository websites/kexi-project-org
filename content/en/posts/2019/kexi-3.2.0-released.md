---
title: "KEXI 3.2.0 Released"
date: "2019-04-09"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 3.2.0 is here, improving stability in the app and frameworks. Since version 3.1.0, KEXI and the frameworks have received about 80 improvements and bug fixes.__

## Highlights

KEXI has greatly improved support for Date, Date/Time, and Time types. Startup improvements refine resource lookup and translations, while updates to the Welcome Page and Table/Form Views ensure proper year formats, integer value display, and robust input validation. Query and report functionalities have been stabilized with crash fixes, parameter support, and better error handling. Import/export processes have been enhanced with improved MS Access data handling, increased binary object size limits, and crash prevention during data transfers.


## MS Windows

Improved MS Windows installer is part of this release.

## Nightly builds

New: Fresh builds are generated every night for Linux/MS Windows.

## Details & Download

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases#3.2.

For general information about downloads, requirements, source code, signatures see <a href="{{< ref "/download" >}}">{{< i18n "download" >}}</a>.

