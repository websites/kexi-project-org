---
title: "KEXI 3.2.0 Beta 1 Released"
date: "2019-01-21"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 3.2.0 Beta 1 and KEXI Frameworks Released with Enhanced Stability. The latest release of KEXI 3.2.0 Beta 1 and its accompanying frameworks brings significant stability improvements to both the application and its core components. Since version 3.1.0, approximately 80 enhancements and bug fixes have been implemented, delivering a more refined and reliable user experience.__

## Highlights

KEXI has greatly improved support for Date, Date/Time, and Time types. Startup improvements refine resource lookup and translations, while updates to the Welcome Page and Table/Form Views ensure proper year formats, integer value display, and robust input validation. Query and report functionalities have been stabilized with crash fixes, parameter support, and better error handling. Import/export processes have been enhanced with improved MS Access data handling, increased binary object size limits, and crash prevention during data transfers.

Among other features, a new date/time notation is now fully supported in user SQL:

![A new date/time SQL notation](images/posts/2019/kexi-3.2-date-constants.png)

Another example is WHERE condition with date constants:

![A new date/time SQL notation](images/posts/2019/kexi-3.2-date-constants-2.png)

More about date-related development on the [blog](https://blogs.kde.org/2019/01/22/kexi-32-beta).

## Details & Download

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases#3.2.

This release offers source code downloads. For Linux binaries users should ask Linux distributors. The source code is known to build and run with Linux (gcc, clang) and MS Windows (msvc>=2013). Some binaries such as installers for Windows 64bit are planned for RC or stable versions.

For general information about downloads, requirements, source code, signatures see <a href="{{< ref "/download" >}}">{{< i18n "download" >}}</a>.

